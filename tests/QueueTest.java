
import static org.junit.Assert.*;
import org.junit.Test;

import model.data_structures.Queue;

public class QueueTest <T extends Comparable<T>>
{
	//---------------------------------
	// CONSTANTES
	//---------------------------------
	
	public static final String OBJETO1 = "obj1";
	public static final String OBJETO2 = "obj2";
	public static final String OBJETO3 = "obj3";
	public static final String OBJETO4 = "obj4";
	public static final String OBJETO5 = "obj5";
	
	//----------------------------------
	// ATRIBUTOS
	//----------------------------------
	
	
	private Queue<T> queue;
	
	
	//----------------------------------
	// METODOS
	//----------------------------------
	
	/**
	 * 
	 * @throws Exception
	 */
	public void setUp() throws Exception
	{
		Queue<T> q = new Queue<T>();
	}
	
	/**
	 * 
	 */
	@Test
	public void testIsEmpty()
	{
		try
		{
			setUp();
			assertTrue("Deberia estar vacio", true);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	@Test
	public void testSize()
	{
		try
		{
			Queue<T> q = new Queue();
			setUp();
			q.enqueue((T) OBJETO1);
			q.enqueue((T) OBJETO2);
			q.enqueue((T) OBJETO3);
			q.enqueue((T) OBJETO4);
			q.enqueue((T) OBJETO5);
			assertEquals(5, q.size());
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testEnQueue()
	{
		try
		{
			Queue<T> q = new Queue();
			setUp();
			q.enqueue((T) OBJETO1);
			q.enqueue((T) OBJETO2);
			q.enqueue((T) OBJETO3);
			q.enqueue((T) OBJETO4);
			
			q.enqueue((T)OBJETO5);
			assertEquals(5, q.size());
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	@Test
	public void testDeQueue()
	{
		try
		{
			Queue<T> q = new Queue();
			setUp();
			q.enqueue((T) OBJETO1);
			q.enqueue((T) OBJETO2);
			q.enqueue((T) OBJETO3);
			q.enqueue((T) OBJETO4);
			
			q.dequeue();
			assertEquals(3, q.size());
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}