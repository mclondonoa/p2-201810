package controller;

import model.data_structures.DoublyLinkedList;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.Queue;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;

import java.time.LocalDate;
import java.time.LocalDateTime;

import API.IManager;


public class Controller {
    private static IManager manager = new Manager();

    public static void cargarDatos(String dataTrips, String dataStations, String dataBikeRoutes){
    	manager.cargarDatos(dataTrips, dataStations, dataBikeRoutes);
    }
    
    public static Queue<Trip> A1(int n, LocalDateTime fechaTerminacion) {
        return manager.A1(n, fechaTerminacion);
    }

    public static DoublyLinkedList<Trip> A2(int n){
        return manager.A2(n);
    }

    public DoublyLinkedList<Trip> A3(int n, LocalDateTime localDateInicio3A) {
        return manager.A3(n, localDateInicio3A);
    }

    public DoublyLinkedList<Bike> B1(int limiteInferior, int limiteSuperior) {
        return manager.B1(limiteInferior, limiteSuperior);
    }

    public DoublyLinkedList<Trip> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
        return manager.B2(fechaInicial, fechaFinal, limiteInferiorTiempo, limiteSuperiorTiempo);
    }

    public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		return manager.B3(estacionDeInicio, estacionDeLlegada);
	}

    public static ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		return manager.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);
	}
    
    public double[] C2(int LA, int LO) {
		return manager.C2(LA, LO);
	}
    
    public class ResultadoCampanna{
    	public int costoTotal;
    	public ILista<Station> estaciones;
	}

	public int darSector(double latitud, double longitud) {
		return manager.darSector(latitud, longitud);
	}

	public DoublyLinkedList<Station> C3(double latitud, double longitud) {
		return manager.C3(latitud, longitud);
	}

	public DoublyLinkedList<BikeRoute> C4(double latitud, double longitud) {
		return manager.C4(latitud, longitud);
	}

	public DoublyLinkedList<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF) {
		return manager.C5(latitudI, longitudI, latitudF, longitudF);
	}

	public static int darCantRoutes() {
		return manager.darCantRoutes();
	}
	
	public static int darCantTrips() {
		return manager.darCantTrips();
	}

	public static int darCantStations() {
		return manager.darCantStations();

	}


}
