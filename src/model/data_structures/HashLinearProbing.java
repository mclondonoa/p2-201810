package model.data_structures;

import java.util.Iterator;

public class HashLinearProbing<K extends Comparable<K>, Value> {
	private int N; // number of key-value pairs in the table
	private int M; // size of linear-probing table
	private K[] keys; // the keys
	private Value[] vals; // the values


	public HashLinearProbing(int m)
	{
		keys = (K[]) new Object[m];
		vals = (Value[]) new Object[m];
		M = m; 
	}
	
	public int getM() {
		return M; 
	}
	
	public int getN() {
		return N; 
	}
	private int hash(K key){ 

		return (key.hashCode() & 0x7fffffff) % M; 
	}
	private void resize(int cap)
	{
		HashLinearProbing<K, Value> t;
		t = new HashLinearProbing<K, Value>(cap);

		for (int i = 0; i < M; i++) {
			if (keys[i] != null)
				t.put(keys[i], vals[i]);
			keys = t.keys;
			vals = t.vals;
			M = t.M;
		}
	}

	public void put(K key, Value val)
	{
		if (N >= M/2) resize(2*M); // double M (see text)
		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % M) {
			if (keys[i].equals(key)) 
			{ 
				vals[i] = val; return; 
			}
			keys[i] = key;
			vals[i] = val;
		}
		N++;
	}

	public Value get(K key)
	{
		for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
			if (keys[i].equals(key))
				return vals[i];
		return null;
	}

	public void delete(K key)
	{
		if (!contains(key)) return;
		int i = hash(key);
		while (!key.equals(keys[i]))
			i = (i + 1) % M;
		keys[i] = null;
		vals[i] = null;
		i = (i + 1) % M;
		while (keys[i] != null){
			K keyToRedo = keys[i];
			Value valToRedo = vals[i];
			keys[i] = null;
			vals[i] = null;
			N--;
			put(keyToRedo, valToRedo);
			i = (i + 1) % M;
		}
		N--;
		if (N > 0 || N == M/8) resize(M/2);
	}

	public boolean contains(K key) {
		return get(key) != null;
	}
	
	public Iterable<K> keys() {
        Queue<K> queue = new Queue<K>();
        for (int i = 0; i < M; i++)
            if (keys[i] != null) queue.enqueue(keys[i]);
        return queue;
    }
	
	public boolean isEmpty() {
		return vals == null; 
	}
	
	public int compareTo() {
		
			
	}
}
