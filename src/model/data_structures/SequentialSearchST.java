package model.data_structures;


public class SequentialSearchST<K extends Comparable<K>, V>
{
	private Node first; 
	private int n;           

	private class Node 
	{
		private K key;
		private V values;
		private Node next;

		public Node(K key, V val, Node next) 
		{
			this.key  = key;
			this.values  = val;
			this.next = next;
		}
	}


	public SequentialSearchST() {
		//Empty symbol table
	}

	public int size() 
	{
		return n;
	}


	public boolean isEmpty() 
	{
		return size() == 0;
	}


	public boolean contains(K key)
	{
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
		return get(key) != null;
	}


	public V get(K key)
	{
		if (key == null) throw new IllegalArgumentException("argument to get() is null"); 
		for (Node x = first; x != null; x = x.next) 
		{
			if ( key.hashCode() == x.key.hashCode())
				return x.values;
		}
		return null;
	}


	public void put(K key, V val) 
	{
		if (key == null) throw new IllegalArgumentException("first argument to put() is null"); 
		if (val == null)
		{
			delete(key);
			return;
		}

		for (Node x = first; x != null; x = x.next)
		{
			if (key.equals(x.key)) 
			{
				x.values = val;
				return;
			}
		}
		first = new Node(key, val, first);
		n++;
	}

	public void delete(K key) 
	{
		if (key == null) throw new IllegalArgumentException("argument to delete() is null"); 
		first = delete(first, key);
	}

	private Node delete(Node x, K key)
	{
		if (x == null) return null;
		if (key.equals(x.key)) 
		{
			n--;
			return x.next;
		}
		x.next = delete(x.next, key);
		return x;
	}



	public Iterable<K> keys() 
	{
		Queue<K> queue = new Queue<K>();
		for (Node x = first; x != null; x = x.next)
			queue.enqueue(x.key);
		return queue;
	}
	
	
}