package model.data_structures;

public class MaxHeapCP<T extends Comparable<T>> {


	private int N= 0;
	private int tamMax;
	private T[] nueva; 

	//Crea un objeto de la clase con una capacidad m�xima de elementos pero vac�a
	public MaxHeapCP(int max) {
		tamMax = max+1; 
		nueva = (T[]) new Comparable[tamMax]; 
	}

	//Retorna n�mero de elementos presentes en la cola de prioridad
	public int darNumElementos() {
		return N; 
	}

	//Agrega un elemento a la cola. Se genera Exception en el caso que se sobrepase el tama�o m�ximo de la cola

	public void agregar(T elemento) throws Exception {
		if(darNumElementos() < tamMax) {
			nueva[++N] = elemento;
			swim(N);
		}
		throw new Exception("Se excedi� el l�mite de elementos");
	}

	//Saca/atiende el elemento m�ximo en la cola y lo retorna; null en caso de cola vac�a
	public T max() {
		T max = null; 
		if(N > 0) {
			max = nueva[1]; // Retrieve max key from top.
			exch(1, N--); // Exchange with last item.
			nueva[N+1] = null; // Avoid loitering.
			sink(1); // Restore heap property.
		}
		return max;
	}

	//Retorna si la cola est� vac�a o no
	public boolean esVacia() {
		return N == 0; 
	}

	//Retorna la capacidad m�xima de la cola
	public int tamanoMax() {
		return tamMax;
	}


	//Auxiliares

	private boolean less(int i, int j){ 
		return nueva[i].compareTo(nueva[j]) < 0; 
	}
	private void exch(int i, int j){ 
		T t = nueva[i]; nueva[i] = nueva[j]; nueva[j] = t; 
	}

	private void swim(int k){
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	private void sink(int k){
		while (2*k <= N){
			int j = 2*k;
			if (j < N && less(j, j+1)) 
				j++;

			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

}
