package model.data_structures;


import java.util.ArrayList;

public class SeparateChainingHashST <K extends Comparable<K>, V >{
		
		private static final int INIT_CAPACITY = 23;
		private int n;   
		private int m;
		private SequentialSearchST<K, V>[] st;


		public SeparateChainingHashST()
		{
			this(INIT_CAPACITY);
		} 
		
		@SuppressWarnings("unchecked")
		public SeparateChainingHashST(int m) 
		{
			n = 0;
			this.m = m;
			st = (SequentialSearchST<K, V>[]) new SequentialSearchST[m];
			for (int i = 0; i < m; i++)
				st[i] = new SequentialSearchST<K, V>();
		} 

		public double factorCarga(){
			return this.size()/m;
		}
		public int getM(){
			return m;
		}
		
		public int getN(){
			return n;
		}
		
		
		private void resize(int chains)
		{
			int capacidad = numPrimo(chains);
			SeparateChainingHashST<K, V> temp = new SeparateChainingHashST<K, V>(capacidad);
			for (int i = 0; i < m; i++) 
			{
				for (K key : st[i].keys()) 
					temp.put(key, st[i].get(key));
			}
			this.m  = temp.m;
			this.n  = temp.n;
			this.st = temp.st;
		}


		public int numPrimo(int pNum)
		{
			int numeroPrimo = pNum+1;
			for (int i = 2; i < numeroPrimo; i++)
			{

				if (numeroPrimo % i == 0) 
				{
					numeroPrimo++;
					i=2;
				}

			}
			return numeroPrimo;
		}

		
		private int hash(K key) 
		{
			return (key.hashCode() & 0x7fffffff) % m;
		} 

		public int size()
		{
			return n;
		} 

		
		public boolean isEmpty()
		{
			return size() == 0;
		}

		
		public boolean contains(K key) 
		{
			if (key == null) throw new IllegalArgumentException("argument to contains() is null");
			return get(key) != null;
		} 

		
		public V get(K key) 
		{
			if (key == null) throw new IllegalArgumentException("argument to get() is null");
	 		int i = hash(key);
			return st[i].get(key);
		} 

		
		public void put(K key, V val)
		{
			boolean agregado = false;

			if (key == null) throw new IllegalArgumentException("first argument to put() is null");
			if (val == null) 
			{
				delete(key);
				agregado = true;
			}
			if(!agregado)
			{
				// double table size if the charge factor is 0.6 or greater.
				if (n/m >= 0.6) resize((2*m));

				int i = hash(key);
				if (!st[i].contains(key)) n++;
				st[i].put(key, val);
			}
			n++;
		} 


		public V delete(K key) 
		{
			V element = null;
			if (key == null) throw new IllegalArgumentException("argument to delete() is null");

			int i = hash(key);
			if (st[i].contains(key)) n--;

			element = st[i].get(key);
			st[i].delete(key);

			if (m > INIT_CAPACITY && n <= 2*m) resize(m/2);

			return element;
		} 

		public Iterable<K> keys() 
		{
			Queue<K> queue = new Queue<K>();
			for (int i = 0; i < m; i++)
			{
				for (K key : st[i].keys())
					queue.enqueue(key);
			}
			return queue;
		} 
		
		public ArrayList<V> values() 
		{
			ArrayList<V> values = new ArrayList<V>();
			for (int i = 0; i < m; i++)
			{
				for (K key : st[i].keys())
				{
					values.add(st[i].get(key));
				}
			}
			return values;
		} 

}

