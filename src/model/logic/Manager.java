package model.logic;

import API.IManager;
import controller.Controller.ResultadoCampanna;
import javafx.scene.Node;
import model.data_structures.DoublyLinkedList;
import model.data_structures.HashLinearProbing;
import model.data_structures.ICola;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.ILista;
import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;
import model.data_structures.QuickSort;
import model.data_structures.RedBlackTree;
import model.data_structures.SeparateChainingHashST;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;
import model.vo.VOSector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Manager implements IManager {
	public static int LONGITUDES = 10;
	public static int LATITUDES = 10;
	public double longitudMaxima;
	public double longitudMinima;
	public double latitudMaxima;
	public double latitudMinima;
	private HashLinearProbing<Integer, VOSector> hashSector;

	//Ruta del archivo de datos 2017-Q1
	// DONE Actualizar
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.txt";

	//Ruta del archivo de trips 2017-Q2
	// DONE Actualizar	
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.txt";

	//Ruta del archivo de trips 2017-Q3
	// DONE Actualizar	
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.txt";

	//Ruta del archivo de trips 2017-Q4
	// DONE Actualizar	
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.txt";

	//Ruta del archivo de stations 2017-Q1-Q2
	// DONE Actualizar	
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.txt";

	//Ruta del archivo de stations 2017-Q3-Q4
	// DONE Actualizar	
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.txt";

	//Ruta del archivo de stations 2017-Q3-Q4
	// DONE Actualizar	
	public static final String BIKE_ROUTES = "data/bikes.json";

	//Carga de datos
	private RedBlackTree<String, BikeRoute> treeRutas; 

	private int cantStations;

	private int cantTrips;

	private int cantRoutes;

	private DoublyLinkedList<Trip> tripsList;

	private HashLinearProbing<Integer, Trip> hashTrips; 

	private RedBlackTree<Integer, Bike> treeBikes; 

	private RedBlackTree<Date, Trip> viajesTree; 

	private HashLinearProbing<Integer, Station> hashStations;  

	private HashLinearProbing<Integer, Trip> grupos; 

	private HashLinearProbing<Integer, Integer> puntos; 

	private VOSector[][] sectores;

	private SeparateChainingHashST hashSC;

	public int darCantRoutes() {
		return cantRoutes;
	}
	public int darCantTrips() {
		return cantTrips;
	}
	public int darCantStations() {
		return cantStations;
	}
	public Manager() {
		cantRoutes=0;
		cantStations=0;
		cantTrips=0;
	}


	@Override
	public void cargarDatos(String rutaTrips, String rutaStations, String dataBikeRoutes) {
		// DONE Auto-generated method stub

		File f = new File(dataBikeRoutes);
		org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
		treeRutas = new RedBlackTree<>();
		try{
			JSONArray arr = (JSONArray) parser.parse(new FileReader(dataBikeRoutes));
			for(Object o : arr){
				JSONObject cicloruta = (JSONObject) o;

				String bikeroute = null;
				bikeroute = (String) cicloruta.get("BIKEROUTE");

				String the_geom = null;
				the_geom = (String) cicloruta.get("the_geom");

				String street = null;
				street = (String) cicloruta.get("STREET");

				String fstreet = null;
				fstreet = (String) cicloruta.get("F_STREET");

				String tstreet = null;
				tstreet = (String) cicloruta.get("T_STREET");

				String shape = null;
				shape = (String) cicloruta.get("Shape_Leng");

				String id = null;
				id = (String) cicloruta.get("id");

				BikeRoute r = new BikeRoute(the_geom, bikeroute, street, fstreet, tstreet, Double.parseDouble(shape));
				cantRoutes++;
				treeRutas.put(id, r);
			}

		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		cargarViajesArbol(rutaTrips);
		cargarHashStations(rutaStations);
		cargarDatos1AHash(rutaStations);
		cargarDatos1ATree(rutaTrips);
	}	

	private void loadBikes(int id, double distance, int duration)
	{
		if(treeBikes.get(id) == null)
		{
			Bike nueva = new  Bike(id);
			nueva.addDistanceTraveled(distance);
			nueva.addDuration(duration);

			treeBikes.put(nueva.getBikeId(), nueva);
		}
		else
		{
			treeBikes.get(id).addDistanceTraveled(distance);
			treeBikes.get(id).addDuration(duration);
		}
	}

	public void cargarViajesArbol (String tripsFile)  {
		hashTrips = new HashLinearProbing<Integer, Trip>(1);

		try{
			File f = new File(tripsFile);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			line = br.readLine();

			while(line != null)
			{
				Trip vo = crearVOTrip(line);
				hashTrips.put(vo.getTripId(), vo);				
				tripsList.add(vo);
				cantTrips++;
				line = br.readLine();
			}


		}catch(Exception e) {
			e.printStackTrace();
		}	
	}



	private Trip crearVOTrip(String line) 
	{
		tripsList = new DoublyLinkedList<>();
		String[] informacion = line.split(",");
		String VOTripId = informacion[0].replace("\"", "");
		String startTime = informacion[1].replace("\"", "");
		String stopTime = informacion[2].replace("\"", "");
		String VOBikeId = informacion[3].replace("\"", "");
		String VOTripDuration = informacion[4].replace("\"", "");
		int startVOStationId = Integer.parseInt( informacion[5].replace("\"", "")  );
		int endVOStationId = Integer.parseInt( informacion[7].replace("\"", "")  );
		String tipo = informacion[9];
		String gender;
		if(informacion.length==10||!(informacion[10].contains("male")||informacion[10].contains("Male")))
		{
			gender = Trip.UNKNOWN;
		}
		else if( informacion[10].contains("fe")||informacion[10].contains("Fe"))
		{
			gender = Trip.FEMALE;
		}
		else
		{
			gender = Trip.MALE;
		}
		double distance = distanciaEstaciones(startVOStationId, endVOStationId);
		Trip elTrip = new Trip(Integer.parseInt(VOTripId), startTime, stopTime, Integer.parseInt(VOBikeId), Integer.parseInt(VOTripDuration), startVOStationId, endVOStationId, gender, tipo, distance);
		int id = elTrip.getBikeId();
		
		int duration = elTrip.getTripDuration();

		loadBikes(id, distance, duration);
		return elTrip;
	}

	private double distanciaEstaciones(int idFrom, int idTo)
	{
		Station primera = (Station) hashSC.get(idFrom);

		Station segunda = (Station) hashSC.get(idTo);

		if(primera != null && segunda != null)
			return Haversine.distance(primera.getLatitude(), primera.getLongitude(), segunda.getLatitude(), segunda.getLongitude());

		else 
			return 0;
	}

	
	public void cargarHashStations(String stationsFile){
		hashSC = new SeparateChainingHashST<Integer, Station>(1);
		try{
			File f = new File(stationsFile);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			line = br.readLine();
			while(line != null )
			{
				Station VOStation = crearVOStation(line);
				hashSC.put(VOStation.getStationId(), VOStation);
				cantStations++;
				line = br.readLine();
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}
	private Station crearVOStation(String line) 
	{
		String[] information = line.split(",");
		String id = information[0].replace("\"", "");
		String name = information[1].replace("\"", "");
		String latitud = information[3].replace("\"", "");
		String longitud = information[4].replace("\"", "");
		String capacity = information[5].replace("\"", "");
		String date = information[6].replace("\"", "");

		Station VOStation = new Station(Integer.parseInt(id), name, date, Integer.parseInt(capacity));

		return VOStation;
	}

	public void cargarDatos1ATree(String file) {
		viajesTree = new RedBlackTree<>();

		try{
			File f = new File(file);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			line = br.readLine();

			while(line != null)
			{
				Trip vo = crearVOTrip(line);
				Instant instant = Instant.from(vo.getStopTime().toLocalDate().atStartOfDay(ZoneId.systemDefault()));
				Date date = Date.from(instant);
				viajesTree.put(date, vo);			


				line = br.readLine();
			}


		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	public void cargarDatos1AHash(String file) {

		hashStations = new HashLinearProbing<Integer, Station>(1);
		try{
			File f = new File(file);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			line = br.readLine();

			while(line != null)
			{
				Station vo = crearVOStation(line);
				hashStations.put(vo.getStationId(), vo);				
				line = br.readLine();
			}


		}catch(Exception e) {
			e.printStackTrace();
		}

	}


	//	public SeparateChainingHashST<Integer, VOSector> crearHashSC(){
	//		BikeRoute r = (BikeRoute) treeRutas.darRoot();
	//		String[] the_geom =r.getTipo().split(",");
	//		double long1 = Double.parseDouble(the_geom[0].split(" ")[0]);
	//		double long2 = Double.parseDouble(the_geom[6].split(" ")[0]);
	//		double lat1 = Double.parseDouble(the_geom[0].split(" ")[1]);
	//		double lat2 = Double.parseDouble(the_geom[6].split(" ")[1]);
	//		int sectorInicial = darSector(long1, lat1);
	//		int sectorFinal = darSector(long2, lat2);
	//		int[] sector =  darCoordenadasSector(sectorInicial);
	//		
	//		if(sectores[sector[0]][sector[1]].getRutas().size()==0){ 
	//			if(sectorInicial==sectorFinal){
	//				sectores[sector[0]][sector[1]].agregarRuta(r);
	//			}else{
	//				int[] sectorini =  darCoordenadasSector(sectorInicial);
	//				int[] sectorfin =  darCoordenadasSector(sectorFinal);
	//				sectores[sectorini[0]][sectorini[1]].agregarRuta(r);
	//				sectores[sectorfin[0]][sectorfin[1]].agregarRuta(r);
	//			}
	//		}
	//		Iterator<BikeRoute> iter = treeRutas.darCola().iterator();
	//		while(iter.hasNext()){
	//			r = iter.next();
	//			the_geom =r.getTipo().split(",");
	//			long1 = Double.parseDouble(the_geom[0].split(" ")[0]);
	//			long2 = Double.parseDouble(the_geom[6].split(" ")[0]);
	//			lat1 = Double.parseDouble(the_geom[0].split(" ")[1]);
	//			lat2 = Double.parseDouble(the_geom[6].split(" ")[1]);
	//			sectorInicial = darSector(long1, lat1);
	//			sectorFinal = darSector(long2, lat2);
	//			sector =  darCoordenadasSector(sectorInicial);
	//			if(sectores[sector[0]][sector[1]].getRutas().size()==0){ 
	//				if(sectorInicial==sectorFinal){
	//					sectores[sector[0]][sector[1]].agregarRuta(r);
	//				}else{
	//					int[] sectorini =  darCoordenadasSector(sectorInicial);
	//					int[] sectorfin =  darCoordenadasSector(sectorFinal);
	//					sectores[sectorini[0]][sectorini[1]].agregarRuta(r);
	//					sectores[sectorfin[0]][sectorfin[1]].agregarRuta(r);
	//				}
	//			}
	//		}
	//		if(hashSC == null){
	//			hashSC = new SeparateChainingHashST<Integer, VOSector>();
	//			for(int i = 0; i < sectores.length; i++){
	//				for(int j = 0; j < sectores[i].length; j++){
	//					VOSector s = sectores[i][j];
	//					Integer id = s.getId();
	//					hashSC.put(id, s);
	//				}
	//			}
	//		}
	//		return hashSC;
	//	}
	//	
	//	
	//	public void ciclorutasHashSC(double longitud, double latitud){
	//		int resp = darSector(longitud, latitud);
	//		if(resp == -1)
	//			System.out.println("Fuera de rango las coordenadas");
	//		else{
	//			VOSector s = (VOSector) hashSC.get(resp);
	//			System.out.println("El punto esta en el rango del sector");
	//			if(s.getRutas().size()==0)
	//				System.out.println("No tiene rutas");
	//			else{
	//				System.out.println("La ruta pasa por "+s.getRutas().get(0).getCalleReferencia()+ " en las coordenadas "+ s.getRutas().get(0).getTipo());
	//			}
	//			Iterator<BikeRoute> iter = s.getRutas().iterator();
	//			BikeRoute r = s.getRutas().get(0);
	//			while(iter.hasNext()){
	//				r = iter.next();
	//				if(estaEnSector(s.getId()))
	//					System.out.println("La ruta pasa por "+r.getCalleReferencia()+ " en las coordenadas "+ s.getRutas().get(0).getTipo());
	//			}
	//		}
	//		
	//	}

	@Override
	public Queue<Trip> A1(int n, LocalDateTime fechaTerminacion) {
		Queue<Trip> req = new Queue<>();
		Instant instant = Instant.from(fechaTerminacion.toLocalDate().atStartOfDay(ZoneId.systemDefault()));
		Date date = Date.from(instant);

		//Recorrer viajes  que terminaron en una estacion con capacidad > o igual a n dada su fecha de terminación. 
		for (Date i: viajesTree.keys()) {
			//Recorro el árbol, dando su llave y esos los añado a una lista
			if(viajesTree.get(i).equals(date)) {
				int id = viajesTree.get(i).getEndStationId(); 
				double capacity = hashStations.get(id).getCapacity(); 

				//Recorro el hash de estaciones dando su llave que esté por encima de n y si cumplen
				if(capacity >= n) {
					req.enqueue(viajesTree.get(i));

					System.out.println("El id del viaje es :"+ viajesTree.get(i).getTripId() + " \n La estación de origen es: " +
							viajesTree.get(i).getStartStationId() + "La estación final es :" + viajesTree.get(i).getEndStationId() +
							" Inicio:" + viajesTree.get(i).getStartTime() + " Finalizo:" + viajesTree.get(i).getStopTime() );
				}
			}

			;


		}


		return req; 
	}

	public void crearTablaHashPorDuracion() {

		grupos = new HashLinearProbing<>(tripsList.size());
		for (int i = 0; i < tripsList.size(); i++) {
			Trip nuevo= tripsList.get(i);
			if(nuevo.getTripDuration() % 60 == 0) {
				grupos.put(nuevo.getTripDuration()/60, nuevo);
			}

			else {
				grupos.put((nuevo.getTripDuration()/60)+1, nuevo);
			}


		}
	}

	@Override
	public DoublyLinkedList<Trip> A2(int n) {
		// TODO Auto-generated method stub
		DoublyLinkedList<Trip> nueva = new DoublyLinkedList<>();
		for (Integer key: grupos.keys()) {

			if((n % 60 == 0) && key == n/60) {
				Trip k = grupos.get(key); 
				nueva.add(k);

				System.out.println("El id del viaje es :"+ k.getTripId() + " \n La estación de origen es: " +
						k.getStartStationId() + "La estación final es :" + k.getEndStationId() + " La duracion es: " + k.getTripDuration() +
						" Inicio:" + k.getStartTime() + " Finalizo:" + k.getStopTime() );

			}

			else if((n % 60 != 0) && key == (n/60)+1){
				Trip k = grupos.get(key); 
				nueva.add(k);

			} 



		}

		return nueva;

		//TODO Falta imprimir
	}


	@Override
	public DoublyLinkedList<Trip> A3(int n, LocalDateTime fecha) {
		// TODO Auto-generated method stub
		DoublyLinkedList lista = new DoublyLinkedList<>();
		try {
			Instant instant = Instant.from(fecha.toLocalDate().atStartOfDay(ZoneId.systemDefault()));
			Date date = Date.from(instant);

			MaxHeapCP orden = new MaxHeapCP<>(viajesTree.size()); //Pasarle el parametro de distancia recorrida


			for (Date key: viajesTree.keys()) {
				Trip nuevo = viajesTree.get(date);
				orden.agregar(nuevo);
			}


			for (int i = 0; i < orden.darNumElementos(); i++) {
				Trip object = (Trip) orden.max();
				lista.add(object);
				System.out.println("El id del viaje es :"+ object.getTripId() + " \n La estación de origen es: " +
						object.getStartStationId() + "La estación final es :" + object.getEndStationId() + 
						" Su longitud es: " + distanciaEstaciones(object.getStartStationId(), object.getEndStationId()) +
						" Inicio:" + object.getStartTime() + " Finalizo:" + object.getStopTime() );
			}
			return lista; 
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return lista; 

		//Falta imprimir los viajes
	}

	@Override
	public DoublyLinkedList<Bike> B1(int limiteInferior, int limiteSuperior) {
		DoublyLinkedList<Bike> bicicletas = new DoublyLinkedList<Bike>();
		for(Integer i: hashTrips.keys()) {
			if(treeBikes.get(i).getDuration() <= limiteSuperior && treeBikes.get(i).getDuration() >= limiteInferior ){
				bicicletas.add(treeBikes.get(i));
			}
		}
		return bicicletas;
	}


	@Override
	public DoublyLinkedList<Trip> B2(String idEstacionInicio, String idEstacionFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		HashLinearProbing<String, DoublyLinkedList<Trip>> tripsHash = new HashLinearProbing<String, DoublyLinkedList<Trip>>(1);
		DoublyLinkedList<Trip> listTrips = new DoublyLinkedList<Trip>();
		for(Integer i: hashTrips.keys()) {
			Trip elTrip = hashTrips.get(i);
			if(tripsHash.get(elTrip.getStartStationId()+"/"+elTrip.getEndStationId())==null){
				DoublyLinkedList<Trip> listaDeTrips = new DoublyLinkedList<>();
				listaDeTrips.add(elTrip);
				tripsHash.put(elTrip.getStartStationId()+"/"+elTrip.getEndStationId(), listaDeTrips);
			}else{
				tripsHash.get(elTrip.getStartStationId()+"/"+elTrip.getEndStationId()).add(elTrip);
			}
		}
		DoublyLinkedList<Trip> tripsEnEstacion = tripsHash.get(idEstacionInicio+"/"+idEstacionFinal);
		for (int i = 0; i < tripsEnEstacion.size(); i++) {
			if(limiteInferiorTiempo <= tripsEnEstacion.get(i).getTripDuration() && limiteSuperiorTiempo >= tripsEnEstacion.get(i).getTripDuration()) {
				listTrips.add(tripsEnEstacion.get(i));
			}
		}
		return listTrips;
	}

	@Override
	public int[] B3(String estacionDeInicioId, String estacionDeLlegadaId) {
		int[][] arr = new int[2][2];
		HashLinearProbing<Integer, Trip> hashTrip = new HashLinearProbing<Integer, Trip>(1);
		for(Integer i: hashTrips.keys()){
			Trip elTrip = hashTrips.get(i);
			if(elTrip.getStartStationId()==Integer.parseInt(estacionDeInicioId) && elTrip.getEndStationId()==Integer.parseInt(estacionDeLlegadaId))
				hashTrip.put(elTrip.getTripId(), elTrip);
			int mayor1 = 0;
			int mayor2 = 0;
			for(Integer k: hashTrips.keys()){
				Trip elTrip1 = hashTrips.get(k);
				if(0 <= hashTrips.keys(i).getFinishDate().getHour() && 1 >= hashTrips.keys(i).getFinishDate().getHour()){
					mayor1++;
					hashTrips.delete(k);
				}else if(0 <= hashTrips.keys(i).getStartDate().getHour() && 1 >= hashTrips.keys(i).getStartDate().getHour()){
					mayor1++;
					hashTrips.delete(k);
				}
			}
			arr = swap(mayor1,0,1,arr);
			for(int j = 1; j < 24; j++){

				for(Integer k: hashTrips.keys()){
					Trip elTrip1 = hashTrips.get(k);
					if(j%24 <= hashTrips.keys(i).getFinishDate().getHour() && (j+1)%24 >= hashTrips.keys(i).getFinishDate().getHour()){
						mayor2++;
						hashTrips.delete(k);
					}else if(j%24 <= hashTrips.keys(i).getStartDate().getHour() && (j+1)%24 >= hashTrips.keys(i).getStartDate().getHour()){
						mayor2++;
						hashTrips.delete(k);
					}
				}
				if(mayor2 > mayor1){
					arr = swap(mayor1,i,i+1,arr);
				}
			}
		}

	}

	private int[][] swap(int x1, int x2, int x3, int[][] a){
		a[0][1] = a[1][1] = x1;
		a[0][0] = x2;
		a[1][0] = x3;
		return a;
	}


	public void puntosEstacion() {
		//Viaje estación final 1 punto
		//Viaje estación inicial 1 punto
		//Además tipo suscriber 1 punto
		puntos = new HashLinearProbing<>(hashStations.getN());
		//Recorrer viajes y sumarle puntos a una estación en un hash según las condiciones

		for (Integer i: hashStations.keys()) {
			int puntosAña = 0; 
			int id = hashStations.get(i).getStationId();


			for (int j = 0; j < tripsList.size(); j++) {
				if(tripsList.get(j).getEndStationId() == id ) {
					puntosAña++; 
				}

				if(tripsList.get(i).getStartStationId() == id) {
					puntosAña++;
				}

				else if(tripsList.get(i).getTipo().equals("Subscriber")) {
					puntosAña++; 
				}
				puntos.put(id, puntosAña);

			}

			//Falta ordenarlos

		}



	}

	@Override
	public double C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		//Recorrer las estaciones hasta n
		//Multiplicar para saber cuanto paga por puntos 
		int indice = 1;
		double valorEstacion = 0;

		for (Integer p : puntos.keys()) {

			if(indice <=numEstacionesConPublicidad){

				valorEstacion += puntos.get(p)*valorPorPunto;
				indice++;

			}
			else {
				return valorEstacion;
			}
		}

		return valorEstacion;
	}

	@Override
	public void C2(int LA, int LO) {
		for(int i = 0; i < 10; ++i){
			double maxL = maxLat() - (i%11)*meanLat();
			double minL = maxLat() - ((i+1)%11)*meanLat();
			for(int j = 0; j < 10; ++j){
				int sectorID = (j+1) + i*10;
				double minLo = minLon() + (j%11)*meanLon();
				double maxLo = minLon() + ((j+1)%11)*meanLon();
				VOSector nuevo = new VOSector(sectorID, minL, maxL, minLo, maxLo);
				hashSector.put(sectorID, nuevo);
			}	
		}

		System.out.println("Numero de sectores: " + hashSector.getM());
		System.out.print("Longitud maxima: " + maxLon() + ", ");
		System.out.println("Longitud minima: " + minLon());
		System.out.print("Latitud maxima: " + maxLat() + ", ");
		System.out.println("Latitud minima: " + minLat() + ", ");

	}

	private String minLat() {
		// TODO Auto-generated method stub
		return null;
	}
	private String maxLon() {
		// TODO Auto-generated method stub
		return null;
	}
	private int meanLon() {
		// TODO Auto-generated method stub
		return 0;
	}
	private int minLon() {
		// TODO Auto-generated method stub
		return 0;
	}
	private int meanLat() {
		// TODO Auto-generated method stub
		return 0;
	}
	private int maxLat() {
		// TODO Auto-generated method stub
		return 0;
	}
	public void crearSectores(){
		sectores = new VOSector[LONGITUDES][LATITUDES];
		double sLong = Math.abs((longitudMaxima-longitudMinima)/LONGITUDES);
		double sLat = Math.abs((latitudMaxima-latitudMinima)/LATITUDES);
		double[] ptsLongitudes = new double[LONGITUDES+1];
		double[] ptsLatitudes = new double[LATITUDES+1];
		ptsLongitudes[0]=longitudMinima;
		ptsLatitudes[0]=latitudMinima;
		for(int i = 1; i<ptsLongitudes.length;i++){
			ptsLatitudes[i] = ptsLatitudes[i-1] - sLat;
			ptsLongitudes[i] = ptsLongitudes[i-1] - sLong;
		}
		for(int i = 1; i < LONGITUDES+1;i++){
			for(int j = 1; j < LATITUDES+1;j++){
				int longitud = i-1;
				int latitud = j-1;
				double longitudMin = ptsLongitudes[i-1];
				double longitudMax = ptsLongitudes[i];
				double latitudMin = ptsLatitudes[j-1];
				double latitudMax = ptsLatitudes[j];
				VOSector s = new VOSector((10*longitud)+latitud+1,  latitudMin, latitudMax, longitudMin, longitudMax);
				sectores[i-1][j-1] = s;
			}
		}
	}

	private boolean estaEnSector(int id){
		for(int i = 0; i < sectores.length; i++){
			for(int j = 0; j < sectores[i].length; j++){
				if(	((10*i)+j+1) == id){
					return true;
				}
			}
		}
		return false;
	}
	private int[] darCoordenadasSector(int sectorInicial) {
		int[] coordenadas = new int[2];
		for(int i = 0; i < sectores.length; i++){
			for(int j = 0; j < sectores[i].length; j++){
				if(	sectorInicial == ((10*i)+j+1)){
					coordenadas[0] = i;
					coordenadas[1] = j;
				}
			}
		}
		return coordenadas;
	}

	@Override
	public int darSector(double latitud, double longitud) {
		int longFinal = -1;
		int latFinal = -1;

		for(int i = 0; i < sectores.length; i++){
			if(longitud > longitudMinima && longitud < longitudMaxima){
				longFinal = i;
				break;
			}
		}
		for(int i = 0; i < sectores[0].length; i++){
			if(latitud > latitudMinima && latitud < latitudMaxima){
				latFinal = i;
				break;
			}
		}
		if(latFinal != -1 && longFinal != -1)
			return (10*longFinal)+latFinal+1;
		return -1;
	}

	@Override
	public HashLinearProbing<VOSector, DoublyLinkedList<Station>> C3(double latitud, double longitud) {
		HashLinearProbing<VOSector, DoublyLinkedList<Station>> tabla = new HashLinearProbing<VOSector, DoublyLinkedList<Station>>(1);
		VOSector buscado =sectordeUnPunto(latitud, longitud);
		if(buscado != null){
			DoublyLinkedList<Station> lista = new DoublyLinkedList<Station>();
			for(Integer i : treeStations){
				Station s = treeStations.get(i);
				if(s.getLatitude() <= buscado.getMaxLat() && s.getLatitude() >= buscado.getMinLat()){
					if(s.getLongitude() <= buscado.getMaxLon() && s.getLongitude() >= buscado.getMinLon()){
						double distance = Haversine.distance(latitud, longitud, s.getLatitude(), s.getLongitude());
						s.setDistanceFrom(distance);
						lista.add(s);
					}
				}
			}
			tabla.put(buscado, lista);
		}

		return tabla;
	}

	private VOSector sectordeUnPunto(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public DoublyLinkedList<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		DoublyLinkedList<BikeRoute> rutas = new DoublyLinkedList<>();
		int resp = darSector(longitud, latitud);
		if(resp == -1)
			System.out.println("Fuera de rango las coordenadas");
		else{
			VOSector s = (VOSector) hashSC.get(resp);
			System.out.println("El punto esta en el rango del sector");
			if(s.getRutas().size()==0)
				System.out.println("No tiene rutas");
			else{
				System.out.println("La ruta pasa por "+s.getRutas().get(0).getCalleReferencia()+ " en las coordenadas "+ s.getRutas().get(0).getTipo());
			}
			Iterator<BikeRoute> iter = s.getRutas().iterator();
			BikeRoute r = s.getRutas().get(0);
			while(iter.hasNext()){
				r = iter.next();
				rutas.add(r);
				if(estaEnSector(s.getId()))
					System.out.println("La ruta pasa por "+r.getCalleReferencia()+ " en las coordenadas "+ s.getRutas().get(0).getTipo());
			}
		}
		return rutas;   
	}

	@Override
	public DoublyLinkedList<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub

		sec
		return null;
	}


	public static class Haversine
	{
		private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

		public static double distance(double startLat, double startLong, double endLat, double endLong) 
		{

			double dLat  = Math.toRadians((endLat - startLat));
			double dLong = Math.toRadians((endLong - startLong));

			startLat = Math.toRadians(startLat);
			endLat   = Math.toRadians(endLat);

			double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

			return EARTH_RADIUS * c;
		}

		public static double haversin(double val) {
			return Math.pow(Math.sin(val / 2), 2);
		}


	}

}


