package model.vo;

import java.time.LocalDateTime;
import java.util.Iterator;

import model.data_structures.DoublyLinkedList;
import model.data_structures.IComparator;
import model.data_structures.QuickSort;

public class Bike implements Comparable<Bike>{

	/**
	 * ID de la bicicleta
	 */
    private int BikeId;
    
    /**
     * Numero total de viajes
     */
    private int totalVOTrips;
    
    /**
     * Distancia recorrida
     */
    
    /**
     * Tiempo de uso
     */
    private int totalDuration;
    
    /**
     * Lista de los viajes de la bicicleta
     */
    private DoublyLinkedList<Trip> listaViajes;
    
    /**
     * Duracion total de uso
     */
    private int duracion;

    private double distancia;
    /**
     * Crea una bicicleta
     * @param BikeId ID de la bicicleta
     */
    public Bike(int BikeId ) 
    {
    	this.distancia=0;
        this.BikeId = BikeId;
        this.totalVOTrips = 0;
        this.totalDuration = 0;
        listaViajes = new DoublyLinkedList<Trip>();
    }

    @Override
    public int compareTo(Bike o) 
    {
    	if( BikeId < o.BikeId)
    		return -1;
    	else if(BikeId == o.BikeId)
    		return 0;
    	else
    		return 1;
    }

    /**
     * Da el ID de la bicicleta
     * @return BikeId
     */
    public int getBikeId() 
    {
        return BikeId;
    }

    /**
     * Da el total de viajes
     * @return Total de viajes
     */
    public int getTotalVOTrips() 
    {
    	return totalVOTrips;
    }


    
    /**
     * Da el tiempo de uso
     * @return Tiempo total de uso
     */
    public int getTotalDuration() 
    {
    	
    	return totalDuration;
    }
    
    /**
     * Anade un viaje a la bicicleta
     */
    public void addTrip(Trip t)
    {
    	listaViajes.addFirst(t);
    }
    
    
    
    public void ordenarViajes()
    {
    	QuickSort<Trip> quick = new QuickSort<Trip>();
    	quick.quickSort(listaViajes.getFirst());
    	
    }
    
    public DoublyLinkedList<Trip> viajesPorFecha(LocalDateTime initial, LocalDateTime end)
    {
    	DoublyLinkedList<Trip> i = new DoublyLinkedList<Trip>();
    	Iterator<Trip> iter = listaViajes.iterator();
    	while( iter.hasNext())
    	{
    		Trip actual = (Trip) iter.next();
    		if ( actual.getStartTime().isAfter(initial) && actual.getStopTime().isBefore(end) )
    		{
    			i.add(actual);
    		}
    	}
    	totalVOTrips = i.size();
    	return i;
    }
    
    public DoublyLinkedList<Trip> getListaViajes() {
		return listaViajes;
	}

    public int getDuration()
    {
    	calcularTotalDeUso();
    	return duracion;
    }
    
    public void calcularTotalDeUso()
    {
    	int tiempo = 0;
    	Iterator<Trip> iter = listaViajes.iterator();
    	while( iter.hasNext() )
    	{
    		Trip actual = iter.next();
    		tiempo += actual.getTripDuration();
    	}
    	duracion = tiempo;
    }
    
    public static class ComparadorBikeXTotalUso implements IComparator<Bike>
    {
    	public int compare(Bike b1, Bike b2)
    	{
    		if(b1.duracion < b2.duracion )
    		{
    			return -1;
    		}
    		else if ( b1.duracion == b2.duracion )
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    }
    
    public static class ComparadorBikeXNumeroViajes implements IComparator<Bike>
    {
    	public int compare(Bike b1, Bike b2)
    	{
    		if(b1.totalVOTrips < b2.totalVOTrips )
    		{
    			return -1;
    		}
    		else if ( b1.totalVOTrips == b2.totalVOTrips )
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    }

	public void addDistanceTraveled(double distance) {
		distancia = distance;
		
	}

	public void addDuration(int pduration) {
			duracion += pduration;		
	}

	
    
    
}