package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private LocalDateTime startDate;
	private double longitude;
	private double latitude;
	private double capacity;
	//TODO Completar

	public Station(int stationId, String stationName, String startDate, double capacity) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.startDate = convertirFecha_Hora_Date(startDate);
		this.capacity = capacity;
	}

	public double getCapacity() {
		return capacity;
	}
	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
	private LocalDateTime convertirFecha_Hora_Date(String date)
	{
		String[] fechaHora = date.split(" ");
		String fecha = fechaHora[0];
		String hora = fechaHora[1];

		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);

		if(datosHora.length == 3)
		{
			int segundos = Integer.parseInt(datosHora[2]);
			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}
		else

		{
			return LocalDateTime.of(agno, mes, dia, horas, minutos);
		}
	}

	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
}
