//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//
// Clase Ruta
//
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------


package model.vo;

public class BikeRoute implements Comparable<BikeRoute>{


	public String theGeom; 

	public String ruta;

	public String calleReferencia;

	public String calleLimUno;

	public String calleLimDos;

	public double longitud;

	public BikeRoute(String ptheGeom, String pRuta, String pCalleReferencia, String pCalleLimiteExtremo1, 
			String pCalleLimiteExtremo2, double pLongitud)
	{
		theGeom = ptheGeom;
		ruta = pRuta;
		calleReferencia = pCalleReferencia;
		calleLimUno = pCalleLimiteExtremo1;
		calleLimDos = pCalleLimiteExtremo2;
		longitud = pLongitud;
	}
	public String getTipo()
	{
		return theGeom;
	}
	
	public String getRuta()
	{
		return ruta;
	}
	
	public void setTipo(String tipo) 
	{
		this.theGeom = tipo;
	}

	

	public void setRuta(String ruta) 
	{
		this.ruta = ruta;
	}

	public String getCalleReferencia()
	{
		return calleReferencia;
	}

	public void setCalleReferencia(String referencia) 
	{
		this.calleReferencia = referencia;
	}

	public String getCalleLimUno() 
	{
		return calleLimUno;
	}

	public void setCalleLimUno(String limiteUno) 
	{
		this.calleLimUno = limiteUno;
	}

	public String getCalleLimDos()
	{
		return calleLimDos;
	}

	public void setCalleLimDos(String limiteDos)
	{
		this.calleLimDos = limiteDos;
	}

	public double getLongitud()
	{
		return longitud;
	}

	public void setLongitud(double pLongitud)
	{
		this.longitud = pLongitud;
	}

	


	@Override
	public int compareTo(BikeRoute arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}