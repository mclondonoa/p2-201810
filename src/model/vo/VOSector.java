package model.vo;

import java.util.ArrayList;

public class VOSector implements Comparable<VOSector> {

	
	private int id; 
	private double longitud; 
	private double latitud;
	public double longitudMaxima;
	public double longitudMinima;
	public double latitudMaxima;
	public double latitudMinima;
	public int cantrutas;
	public ArrayList<BikeRoute> rutas;
	public VOSector(int pId, double pLatMin, double pLatMax, double pLongMin, double pLongMax) {
		id = pId;

		latitudMinima = pLatMin;
		latitudMaxima = pLatMax;
		longitudMaxima = pLongMax;
		longitudMinima = pLongMin;
		cantrutas = 0;
		rutas = new  ArrayList<BikeRoute>();
	}
	
	public int getId() {
		return id; 
	}
	
	public void agregarRuta(BikeRoute r){
		rutas.add(r);
		cantrutas++;
	}
	public double getLong() {
		return longitud;
	}
	
	public double getLat() {
		return latitud; 
	}
	
	
	public ArrayList<BikeRoute> getRutas(){
		return rutas;
	}
	
	@Override
	public int compareTo(VOSector arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}
