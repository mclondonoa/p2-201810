package model.vo;

import java.time.LocalDateTime;

import model.data_structures.IComparator;

public class Trip implements Comparable<Trip> {

	public final static String MALE = "male";
	public final static String FEMALE = "female";
	public final static String UNKNOWN = "unknown";

	private int tripId;
	private LocalDateTime startTime;
	private LocalDateTime stopTime;

	private int bikeId;
	private int tripDuration;
	private int startStationId;
	private int endStationId;
	private String gender;
	private boolean stationf ;
	private String tipo; 
	private double distance;


	public Trip(int tripId, String startTime, String stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender, String tipo, double distance) {
		this.tripId = tripId;
		this.startTime = convertirFecha_Hora_Date(startTime);
		this.stopTime = convertirFecha_Hora_Date(stopTime);
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.startStationId = startStationId;
		this.endStationId = endStationId;
		this.gender = gender;
		this.stationf = false;
		this.tipo = tipo;
		this.distance = distance;

	}

	@Override
	public int compareTo(Trip o) {
		// TODO completar
		return 0;
	}

	public String getTipo() {
		return tipo; 
	}

	public int getTripId() {
		return tripId;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getStopTime() {
		return stopTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public int getStartStationId() {
		return startStationId;
	}

	public int getEndStationId() {
		return endStationId;
	}

	public String getGender() {
		return gender;
	}
	private LocalDateTime convertirFecha_Hora_Date(String date)
	{
		String[] fechaHora = date.split(" ");
		String fecha = fechaHora[0];
		String hora = fechaHora[1];

		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);

		if(datosHora.length == 3)
		{
			int segundos = Integer.parseInt(datosHora[2]);
			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}
		else

		{
			return LocalDateTime.of(agno, mes, dia, horas, minutos);
		}
	}
	public void changeStF(boolean valor)
	{
		stationf = valor;

	}

	public boolean isStationfC() {
		return stationf;
	}

	public static class ComparadorXHoraEnd implements IComparator<Trip>
	{
		public int compare(Trip t1, Trip t2)
		{
			return t1.getStopTime().compareTo(t2.getStopTime());
		}
	}
	public static class ComparadorXHoraEndXhoraInical implements IComparator<Trip>
	{
		public int compare(Trip t1, Trip t2)
		{
			if(t1.stationf&&t2.stationf)
				return t1.getStopTime().compareTo(t2.getStopTime());
			else if(t1.stationf&&!t2.stationf)
				return t1.getStopTime().compareTo(t2.getStartTime());
			else if(!t1.stationf&&t2.stationf)
				return t1.getStartTime().compareTo(t2.getStopTime());
			else
				return t1.getStartTime().compareTo(t2.getStartTime());				
		}
	}

	public static class ComparadorDistancia implements IComparator<Trip>
	{
		public int compare(Trip t1, Trip t2)
		{
			if(t1.distance == t2.distance)
			return 0;
			
			else if(t1.distance < t2.distance) {
				return -1;
			}
			else {
				return 1;
			}
		}
	}

}

